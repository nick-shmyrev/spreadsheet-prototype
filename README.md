# spreadsheet-prototype
Web Apps & Tech college course project. 

## Requirements summary
Build a web application that allows users to create and edit simplified spreadsheets running within a web browser. Your solution can be created using Visual Studio or a standalone HTML web page (using as well CSS and JavaScript). The application must be written to manipulate the spreadsheet client using JavaScript, CSS and the DOM.
The only formula you will need to handle will be =SUM(Ax:By) where “A” and “B” represent from and to columns and “x” and “y” represent from and to rows – as with MS Excel. The objective being to sum a set of cells and display the result. For the purpose of this project, you can assume that all Ax and Bx coordinates will represent a column or row of numbers.
See "INFO3069 Spreadsheet Project_2017.pdf" for the [full list of requirements](https://github.com/Nick-Shmyrev/spreadsheet-prototype/blob/master/INFO3069%20Spreadsheet%20Project_2017.pdf).

## See it in action
**[Click.](https://htmlpreview.github.io/?https://github.com/Nick-Shmyrev/spreadsheet-prototype/blob/master/index.html)**
