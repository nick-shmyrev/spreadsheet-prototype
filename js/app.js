/*
██ ███    ██ ██ ████████
██ ████   ██ ██    ██
██ ██ ██  ██ ██    ██
██ ██  ██ ██ ██    ██
██ ██   ████ ██    ██
*/

/*jshint esversion: 6 */

const btnSave = document.getElementById('btnSave');
const btnLoad = document.getElementById('btnLoad');
const btnClear = document.getElementById('btnClear');
const table = document.getElementById('table');
const txtInput = document.getElementById('txtInput');
const form = document.getElementById('form');
let dataArray = cleanArray();


btnSave.addEventListener('click', saveTableData);
btnLoad.addEventListener('click', loadTableData);
btnClear.addEventListener('click', clearTable);
txtInput.addEventListener('blur', updateCells);
form.addEventListener('submit', function(event) {
  event.preventDefault();
  updateCells();
});

renderTableRows(table);
loadTableData();

/*
███████ ██    ██ ███    ██  ██████ ████████ ██  ██████  ███    ██ ███████
██      ██    ██ ████   ██ ██         ██    ██ ██    ██ ████   ██ ██
█████   ██    ██ ██ ██  ██ ██         ██    ██ ██    ██ ██ ██  ██ ███████
██      ██    ██ ██  ██ ██ ██         ██    ██ ██    ██ ██  ██ ██      ██
██       ██████  ██   ████  ██████    ██    ██  ██████  ██   ████ ███████
*/


/**
 * Saves table data to local storage
 * @method saveTableData
 */
function saveTableData() {
  const storage = window.localStorage;

  storage.setItem('appData', JSON.stringify(dataArray));
  console.log('data saved');
} // END saveTableData()


/**
 * Loads table data from local storage
 * @method loadTableData
 */
function loadTableData() {
  const storage = window.localStorage;

  clearTable();
  dataArray = JSON.parse(storage.getItem('appData')) || cleanArray();

  for (let row = 0; row < 20; row++) {
    for (let column = 0; column < 10; column++) {
      let cell = document.getElementById(`${row}.${column}`);
      cell.textContent = cellEval(dataArray[row][column]);
    }
  }
  console.log('data loaded');
} // END loadTableData()


/**
 * Clears html table and of any values, replaces dataArray[] with an empty array
 * @method clearTable
 */
function clearTable() {
  const cellsCollection = document.querySelectorAll('.tblCell');

  txtInput.value = '';
  cellsCollection.forEach(function (element) {
    element.textContent = '';
  });

  dataArray = cleanArray();
} // END clearTable()


/**
 * Takes a reference to parent table, generates html markup for 20 table rows, 10 cells per row
 * @method renderTableRows
 * @param  {object}        parent Reference to a parent table
 */
function renderTableRows(parent) {
  // rows loop
  for (let rowNum = 0; rowNum < 20; rowNum++) {
    let row = document.createElement("tr");
    let rowHeader = document.createElement("th");

    rowHeader.textContent = rowNum + 1;
    rowHeader.setAttribute('class', 'tblHeading');

    row.appendChild(rowHeader);

    // columns loop
    for (let colNum = 0; colNum < 10; colNum++) {
      let td = document.createElement("td");

      td.setAttribute('class', 'tblCell');
      td.setAttribute('id', `${rowNum}.${colNum}`);
      td.addEventListener('click', resetActiveCell);

      row.appendChild(td);
    } // END coumns loop

    parent.appendChild(row);
  } // END rows loop

} // END renderTableRows()


/**
 * Resets the active cell to current selection, reads cell data from dataArray[] to the txtInput
 * @method resetActiveCell
 */
function resetActiveCell() {
  let [activeCell] = table.getElementsByClassName('active');
  if (activeCell) {
     activeCell.classList.remove('active');
  }
  this.classList.add('active');
  const [row, cell] = this.id.split('.');
  txtInput.value = dataArray[row][cell];
  txtInput.focus();
}


/**
 * Generates an empty 2d-array of 20 rows, 10 cells per row
 * @method cleanArray
 * @return {array}   Empty array[20][10]
 */
function cleanArray() {
  let array = [];

  for (let row = 0; row < 20; row++) {
    array[row] = [];

    for (let cell = 0; cell < 10; cell++) {
      array[row][cell] = '';
    }
  }
  return array;
}


/**
 * Updates every cell of a table
 * @method updateCells
 */
function updateCells() {
  const [activeCell] = table.getElementsByClassName('active');

  if (activeCell) {
    const [row, cell] = activeCell.id.split('.');

    dataArray[row][cell] = txtInput.value;

    for (let row = 0; row < 20; row++) {
      for (let column = 0; column < 10; column++) {
        let cell = document.getElementById(`${row}.${column}`);
        cell.textContent = cellEval(dataArray[row][column]);
      }
    }
  }
} // END updateCells()


/**
 * Returns result of a calculation if result is a number, or initial cell data
 * @method cellEval
 * @param  {string} cellData Cell data
 * @return {string}          Result of calculation if it's a number, or initial cell data
 */
function cellEval(cellData) {

  if (cellData.charAt(0) === '=') {
    return formulaParse(cellData);
  } else {
    try {
      let calcResult = eval(cellData); // Yes, "eval() is evil", however writing a fully-fledged parser would be too time-consuming and beyond the scope of this project
      if (!isNaN(calcResult)) {
        return calcResult.toString();
      }
    } catch(err) {
      return cellData;
    }
  }
}


/**
 * Parses a formula into formula type, startCell & endCell
 * @method formulaParse
 * @param  {string}     cellData Cell data containing formula
 * @return {function}            Returns result of cellSum()
 */
function formulaParse(cellData) {
  const formulaPattern = /^=([A-Z]+)\(([A-Z][\d]+):([A-Z][\d]+)\)$/i;

  if (cellData.match(formulaPattern)) {
    const [ ,type, startCell, endCell] = cellData.match(formulaPattern);

    if (type.toUpperCase() !== 'SUM') {
      return '#Unknown formula type';
    } else {
      return cellSum(startCell, endCell);
    }
  }
  else {
    return '#Formula Syntax Error';
  }
}


/**
 * Takes the range of cells to sum, returns the sum of the cell values. Currently only accepts either a row or a column as a range
 * @method cellSum
 * @param  {string} startCell Index of a starting cell
 * @param  {string} endCell   Index of an ending cell
 * @return {number}           Sum of cell values
 */
function cellSum(startCell, endCell) {

  if (startCell.slice(1) === endCell.slice(1)) {
    // sum row
    const row = startCell.slice(1) - 1;
    let startCol = startCell.slice(0, 1).toUpperCase().charCodeAt(0) - 65;
    let endCol = endCell.slice(0, 1).toUpperCase().charCodeAt(0) - 65;
    let result = 0;

    // if cells range starts from the end, switch the startCol & endCol to still return the correct result
    if (startCol > endCol) {
      [startCol, endCol] = [endCol, startCol];
    }

    for (let col = startCol; col <= endCol; col++) {
      result += parseFloat(dataArray[row][col]);
    }
    return result;

  } else if (startCell.slice(0, 1) === endCell.slice(0, 1)) {
    // sum column
    const col = startCell.slice(0, 1).toUpperCase().charCodeAt(0) - 65;
    let startRow = startCell.slice(1) - 1;
    let endRow = endCell.slice(1) - 1;
    let result = 0;

    // if cells range starts from the end, switch the startRow & endRow to still return the correct result
    if (startRow > endRow) {
      [startRow, endRow] = [endRow, startRow];
    }

    for (let row = startRow; row <= endRow; row++) {
      result += parseFloat(dataArray[row][col]);
    }
    return result;

  } else {
    return '#Err';
  }
}
